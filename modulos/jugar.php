<?php ?>

<div class="container-fluid" id="jugar" >
    <?php
     if(!isset($_GET["id_partida"])){
        ?>
                <div class="row">
        <div class="col-md-4">
            <br>
            <button class="m-10 mt-10 mb-10"  type="button" :class="['btn btn-info']">
                <H1>CREAR NUEVA PARTIDA</H1>
            </button>
            <br><br>
            
                <label for="inputAddress">NOMBRE DE LA PARTIDA</label>
                <input type="text" v-model="NombreNuevaPartida" :class="['form-control', Alert_Nombre_Partida]" 
                @click="Alert_Nombre_Partida = 'border-default'">
                <br>
                <button @click="RegistrarNuevaPartida()" type="button" class="btn btn-success mt-20 mb-50">Crear partida</button>
                <br><br><br>
            
            
        </div>
        <div class="col-md-8">
            <div v-if="NUmpartidasExistentes > 0"  >
                <br>
                <H4 >TUS PARTIDAS</H4> <br>
                <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                    <div v-for="(partida, index) in partidasDB">
                       
                        <button style="margin-bottom: 2px;
                            margin-left: 2px;
                            margin-right: 2px;
                            margin-top: 2px;" type="button" :class="[partida.class_estilo]"
                            @click="JugarPartida(partida.id)">
                                <h2>{{partida.titulo}}</h2> 
                                <span class="badge badge-light">{{partida.estado}} - {{partida.fecha_registro}}</span>
                        </button>
                       
                    </div>
                </div>   
            </div>
        </div>

        <!-- LISTA DE JUGADAS -->
    </div>
        <?php
     }else{
         require_once "Controladores/php/ctrlPartidasINDEX.php";
        
         ?>
            <div class="row">
                <div class="col-md-3">
                    <br>
                    <button class="m-10 mt-10 mb-10 <?php echo $estilosBadge[$partida["estado"]]; ?> "  type="button" >
                        <H1><?php echo $partida["titulo"] ; ?></H1>
                        <span class="badge badge-light"><?php echo $partida["fecha_registro"] ?></span>
                    </button>
                    <br><br>
                    
                        
                   <?php 
                    if($partida["estado"] == "finalizada"){
                        ?><div class="d-flex justify-content-center" >
                        <img src="dist/img/bingo-title.png" class="img-fluid" alt="Responsive image">
                    </div> <?php
                    }
                    else if ($partida["estado"] !== "finalizada") {
                        ?> 
                             <div>
                        <div class="form-row">
                            <h4>INGRESAR BALOTA</h4>
                                <div class="form-group col-md-6">
                                <label for="inputEmail4">LETRA </label>
                                <input id="id_partida_" type = "hidden" value="<?php echo $_GET["id_partida"] ; ?>"> 
                                <input id="estado_partida_" type = "hidden" value="<?php echo $partida["estado"] ; ?>"> 

                                <select v-model="LetraBalota" :class="['form-control', Alert_LetraBalota]" 
                                @click="Alert_LetraBalota = 'border-default'">
                                    <option value="">----</option>
                                    <option value="B">B</option>
                                    <option value="I">I</option>
                                    <option value="N">N</option>
                                    <option value="G">G</option>
                                    <option value="O">O</option>
                                </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputAddress">NÚMERO</label>
                                    <input type="number" v-model="NumeroBalota" :class="['form-control', Alert_NumeroBalota]" 
                                    @click="Alert_NumeroBalota = 'border-default'">
                                </div>
                            </div>

                            <button @click="IngresarBalota()" type="button" class="btn btn-success mt-20 mb-50">
                            ACEPTAR</button>
                    </div>
                        <?php
                    }
                   ?>
        
                    <br><br><br>    
                </div>





                
                <div class="col-md-3">
                    <div>
                        <br>
                        <H4 >BALOTAS JUGADAS <?php echo count($BalotasPartidas); ?></H4> <br>
                        
                        <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                            
                                <?php                         
                                    foreach($BalotasPartidas as $balota){
                                        ?>
                                        <div class="col-md-2" style="margin-bottom: -5000px; margin-left:2px;  margin-right:2px;" >
                                            <span style="cursor: pointer; "  @click="BalotaInfo('<?php echo $balota["letra"] ?>','<?php echo $balota["numero"] ?>', '<?php echo $balota["id_partida"] ?>') "
                                            class="badge badge-pill badge-primary">
                                            <h3><?php echo $balota["letra"].$balota["numero"] ?></h3>
                                            </span>   
                                        </div>
                                        <?php
                                    }
                                ?>
                            
                        </div>   
                    </div>
                </div>

                
                <!-- MOMENTOS DE LA JUGADA --> 
                <!-- aquí pondré datos como: numero de tablas en las que se encuentra la balota
                jugadores favorecidos -->
                <div class="col-md-3">
                    <br>
                    
            
                    <div >
                        <H5>Información balota <?php echo @$_GET["letra"].@$_GET["numero"]; ?></H5>
                        
                        <p>Tablas: <?php echo @$NumTablas; ?></p> 
                            <p> Jugadores: <?php echo @$numJugadores; ?></p>
                        
                        
                            
                        

                        <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                            <?php 

                            if (isset($_GET["balota-seleccionada"])) {
                                foreach($jugadores_que_tienen_las_tablas as $jugador){
                                    ?> 
                                    <div  class="alert alert-primary" role="alert">
                                    <?php echo $jugador["nombre"] ?>
                                    <a href="#" class="alert-link">Tabla: <?php echo $jugador["numero_tabla"] ?></a>. 
                                    Direección: <?php echo $jugador["direccion"]?>
                                    Fecha registro:  <?php echo $jugador["fecha_registro"] ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    

                    
                </div>


                
                <!-- SABER QUIENES VAN GANANDO (PREDICCIÓN) -->
                <div class="col-md-3">
                    <br>
                    
                    <H5>AVANCE JUGADORES</H5>

                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=1";?>">L1</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=2";?>">L2</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=3";?>">L3</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=4";?>">L4</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=5";?>">L5</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=6";?>">L6</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=7";?>">L7</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=8";?>">L8</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=9";?>">L9</a>|
                    <a href="index.php?rute=jugar&id_partida=<?php echo $_GET["id_partida"]."&avance-jugadores&lote_tabla=10";?>">L10</a>
                    
                    <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                    <?php 
                       
                       if (isset($_GET["avance-jugadores"])) {
                        require_once "Controladores/php/BINGO.php";
                        $tablas_a_mostrar = array();
                        foreach ($tablas_numero_veces_cantadas as $key => $tabla) {
                           
                           if ($tabla["veces"] == 24) {
                            echo "<p class='text-danger'>TABLA: ". $tabla["tabla"]. " - VECES: ". $tabla["veces"]."</p><br>";
                           }else{
                            echo "TABLA: ". $tabla["tabla"]. " - VECES: ". $tabla["veces"]."<br>";
                           }
                           
                            
                            
                        }
                       }
                    ?>
                    </div>
                </div>


                

            </div>
         <?php
     }

     ?>


   
</div>

<script src="Controladores/js/Jugar.js"></script>