<?php
require_once "../../Modelos/conexion.php";
require_once "../../Modelos/mdlJugadores.php";
require_once "../../Modelos/tablas.php";
require_once "../../ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();



if (isset($_POST["registrarJugador"])) {

    $data = file_get_contents("../../Modelos/DB/DB_jugadores.json");
    $JugadoresDB = json_decode($data, true);

    ## encapsulamiento de datos que vienen del formulario 
    $datos = array(
        "nombre" => $_POST["nombre"],
        "num_cedula" => $_POST["num_cedula"],
        "direccion" => $_POST["direccion"],
        "telefono" => $_POST["telefono"],
        "numero_tabla" => $_POST["numero_tabla"],
        "fecha_registro" => $fechaActual
    );


    ## validamos que el numero de la tabla ingresado si exista
    $ValidarExistenciaNumTabla = false;
    foreach ($tablas as $key => $tabla) {
        if ($tabla["codigo_tabla"] == $datos["numero_tabla"]) {
            $ValidarExistenciaNumTabla = true;
            break;
        }
    }
    if ($ValidarExistenciaNumTabla) { ## si si existe el num de la tabla digitada
        ## si existe entonces debemos validar que ya ese numero de tabla no esté asignado a otro jugador
        //$validarDisponibilidadDeTabla = mdlJugadores::CargarJugadores("numero_tabla", $datos["numero_tabla"], "DESC", "id");
        $validarDisponibilidadDeTabla = false;

        foreach($JugadoresDB as $JDB){
            if($JDB["numero_tabla"] == $datos["numero_tabla"]){
                $validarDisponibilidadDeTabla = true;
                break;
            }
        }

        if (!$validarDisponibilidadDeTabla) { ## si no encuentra un registro pues quiere decir que está disponible y procedemos a registrar:
            ## pero antes debemos verificar que el jugador no esté repetido .. un jugador no puede tener dos tablas_bingo
            //$ConsultarJugador = mdlJugadores::CargarJugadores("num_identificacion", $datos["num_cedula"], "DESC", "id");
            $ConsultarJugador = false;
            foreach($JugadoresDB as $JDB){
                if($JDB["num_identificacion"] == $datos["num_cedula"]){
                    $ConsultarJugador = true;
                    break;
                }
            }

            if (!$ConsultarJugador) { ## si no encuentra el jugadro pues ... es porque no existe asi que lo registraremos
                    //$registrar = mdlJugadores::RegistrarJugador($datos);
                
                //recorremos los jugadores para tomar el último y asi saber el id 
                // del nuevo jugador que voy a registrar
                foreach($JugadoresDB as $JDB){}
                $id_nuevoJugador = $JDB["id"]+1;

                ## creo un nuevo array u objeto con los datos del nuevo jugador
                $nuevo_jugador = array(
                    "id" =>$id_nuevoJugador,
                    "nombre" =>$datos["nombre"],
                    "num_identificacion" => $datos["num_cedula"],
                    "telefono" => $datos["telefono"],
                    "direccion" => $datos["direccion"],
                    "numero_tabla" => $datos["numero_tabla"],
                    "fecha_registro" => $fechaActual
                );

                ## yy ahora lo inserto en el array que tenemos como base de datos:
                array_push($JugadoresDB , $nuevo_jugador);

                ## ahora actualizo la base de datos (archivo JSON)
                $jsonencoded = json_encode($JugadoresDB,JSON_UNESCAPED_UNICODE);
                $registrar = fopen("../../Modelos/DB/DB_jugadores.json", 'w');
                fwrite($registrar, $jsonencoded);
                fclose($registrar);


                if ($registrar) {
                    $rest["respuesta"] = "ok";
                }else{
                    $rest["respuesta"] = "err";
                }
            }else{
                $rest["respuesta"] = "JugadorExistente";        
            }
        }else{
            $rest["respuesta"] = "NumTablaTomado";    
        }
    
    }else{
        $rest["respuesta"] = "NumTablaNoEncontrado";
    }




   
    


    header("Content-Type: application/json");
    echo json_encode($rest);
}



















if (isset($_GET["CargarJugadores"])) {
    if ($_GET["CargarJugadores"] == "CargarTodo") {
        //$Jugadores = mdlJugadores::CargarJugadores(null, null, "DESC", "id");
    
        $data = file_get_contents("../../Modelos/DB/DB_jugadores.json");
        $Jugadores = json_decode($data, true);

        rsort($Jugadores, SORT_REGULAR);

        $rest["respuesta"] = $Jugadores;
                
    }



    header("Content-Type: application/json");
    echo json_encode($rest);
}