<?php

class mdlJugadores{


    static public function RegistrarJugador($datos){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        jugadores(nombre, num_identificacion, telefono, direccion, numero_tabla, fecha_registro) 
        VALUES (:nombre, :num_identificacion, :telefono, :direccion, :numero_tabla, :fecha_registro)");
        $stmt->bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
        $stmt->bindParam(":num_identificacion", $datos["num_cedula"], PDO::PARAM_STR);
        $stmt->bindParam(":telefono", $datos["telefono"], PDO::PARAM_STR);
        $stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
        $stmt->bindParam(":numero_tabla", $datos["numero_tabla"], PDO::PARAM_STR);
        $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
        
        

        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
        $stmt = null;       
    }



    static public function CargarJugadores($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  jugadores WHERE $item = $value ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadores ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadores ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }
}