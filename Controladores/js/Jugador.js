var Jugador = new Vue({
    el: "#Jugadores",
    data : {
        Loader: false,

        JugadoresDB: [],
        jugadoresEncontrados: [],
        JugadoresQueSeMuestranDB:[],

        buscar: "",
        buscarT: "",

         ///DATOS PARA REGISTRAR JUGADOR : :: 
        nombre: "",
        num_cedula: "",
        telefono: "",
        direccion: "",
        numero_tabla: "",

        //ALERTAS 
        ALert_nombre: "border-default",
        Alert_num_cedula: "border-default",
        Alert_telefono: "border-default",
        Alert_direccion: "border-default",
        Alert_numero_tabla: "border-default",
        AlertJugadorRegistrado: false,
        AlertNumTablaNoEncontrado: false,
        AlertTablaTomada: false,
        AlertJugadorExistente: false
    },
    methods: {

        CargarJugadores: function(){
            this.Loader = true;
            axios.get("Controladores/php/ctrlJugadores2.php?CargarJugadores=CargarTodo")
            .then(function(response){
                let datos = response.data;
                Jugador.JugadoresDB = datos.respuesta;
                Jugador.JugadoresQueSeMuestranDB = datos.respuesta;
                Jugador.Loader = false;
                console.log(response);
            })
        },


        RegistrarJugador: function(){
            this.Loader = true;
            this.AlertJugadorRegistrado =  false;
            this.AlertNumTablaNoEncontrado =  false;
            this.AlertTablaTomada = false;
            this.AlertJugadorExistente =  false;

            if(this.nombre !== "" && this.num_cedula !== "" && this.direccion !== "" &&
            this.telefono !== "" && this.numero_tabla !== ""){
                formdata = new FormData();
                formdata.append("registrarJugador", "ok");
                formdata.append("nombre", this.nombre);
                formdata.append("num_cedula", this.num_cedula);
                formdata.append("direccion", this.direccion);
                formdata.append("telefono", this.telefono);
                formdata.append("numero_tabla", this.numero_tabla);


                axios.post("Controladores/php/ctrlJugadores2.php", formdata).then(function(response){
                    let datos = response.data;
                    console.log(response);

                    if(datos.respuesta == "ok"){
                        Jugador.MostrarAlertas("registrarJugador", "ok");
                        // formateamos
                        Jugador.nombre = "";
                        Jugador.num_cedula = "";
                        Jugador.telefono =  "";
                        Jugador.direccion = "";
                        Jugador.numero_tabla = "";
                        Jugador.CargarJugadores();
                        Jugador.Loader = true;

                    }else if(datos.respuesta == "NumTablaNoEncontrado"){
                        Jugador.MostrarAlertas("registrarJugador", "NumTablaNoEncontrado");
                        Jugador.Loader = true;
                    }else if(datos.respuesta == "NumTablaTomado"){
                        Jugador.MostrarAlertas("registrarJugador", "NumTablaTomado");
                        Jugador.Loader = true;
                    }else if(datos.respuesta == "JugadorExistente"){
                        Jugador.MostrarAlertas("registrarJugador", "JugadorExistente");
                        Jugador.Loader = true;
                    }
                })
            }else{
                this.MostrarAlertas("registrarJugador", "camposVacios");
                this.Loader = false;
            }
        },

        MostrarAlertas: function(tipo, estado){
            if(tipo == "registrarJugador"){
                if(estado == "camposVacios"){
                    if(this.nombre == ""){this.ALert_nombre = "border-danger";}
                    if(this.num_cedula == ""){this.Alert_num_cedula = "border-danger";}
                    if(this.direccion == ""){this.Alert_direccion = "border-danger";}
                    if(this.telefono == ""){this.Alert_telefono = "border-danger";}
                    if(this.numero_tabla == ""){this.Alert_numero_tabla = "border-danger";}
                }else if(estado == "ok"){
                    this.AlertJugadorRegistrado = true;
                }else if(estado == "NumTablaNoEncontrado"){
                    this.AlertNumTablaNoEncontrado = true;
                    this.Alert_numero_tabla = "border-danger";
                }else if(estado == "NumTablaTomado"){
                    this.Alert_numero_tabla = "border-danger";
                    this.AlertTablaTomada = true;
                }else if(estado == "JugadorExistente"){
                    this.Alert_num_cedula = "border-danger";
                    this.AlertJugadorExistente = true;
                }
            }
        },

        BuscarCedula: function()
        {
            this.buscarT = "";
            if(this.buscar == "")
            {
            this.CargarJugadores();
            //console.log("vacio");
            }else{
            //convertimos el texto que está ingresando 
            //el usuario a minusculas para que sea más fácil realizar el filtro
            const texto = Jugador.buscar.toLowerCase();
            this.jugadoresEncontrados.splice(0);
            
            for(let _jugador_ of this.JugadoresDB )
                {
                    let _jugador = _jugador_["num_identificacion"];
                    let _infoJugador = _jugador.toLowerCase();
                    //console.log(_infoJugador);
                
                    if(_infoJugador.indexOf(this.buscar) !== -1){
                            this.jugadoresEncontrados.push(_jugador_);
                            this.JugadoresQueSeMuestranDB = this.jugadoresEncontrados;
                    }
                }
            }
                
        },

        BuscarTabla: function()
        {
            this.buscar = "";
            if(this.buscarT == "")
            {
            this.CargarJugadores();
            //console.log("vacio");
            }else{
            //convertimos el texto que está ingresando 
            //el usuario a minusculas para que sea más fácil realizar el filtro
            const texto = Jugador.buscarT.toLowerCase();
            this.jugadoresEncontrados.splice(0);
            
            for(let _jugador_ of this.JugadoresDB )
                {
                    let _jugador = _jugador_["numero_tabla"];
                    let _infoJugador = _jugador.toLowerCase();
                    //console.log(_infoJugador);
                
                    if(_infoJugador.indexOf(this.buscarT) !== -1){
                            this.jugadoresEncontrados.push(_jugador_);
                            this.JugadoresQueSeMuestranDB = this.jugadoresEncontrados;
                    }
                }
            }
                
        },


    },

    

    mounted(){
        this.CargarJugadores();

    }

})