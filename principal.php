<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="Assets/jquery.min.js"></script>


    <script src="Assets/Vue.js"></script>
    <script src="Assets/Axios.js"></script>
    
  </head>


  <body>
    
    <div class="container-fluid" id="Bingo">

      <!-- IMAGEN CABECERA -->
        <div class="d-flex justify-content-center">
            <img src="dist/img/header.gif" class="img-fluid" alt="Responsive image">
        </div>


      <!-- MENÚ -->
        <div class="d-flex  justify-content-center">
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" >
               <a href="estadisticas" class="nav-link active"    aria-selected="true">Estadísticas</a>
            </li>
            <li class="nav-item" >
               <a href="jugar" class="nav-link"    aria-selected="true">Jugar</a>
            </li>
            <li class="nav-item" >
               <a href="jugadores" class="nav-link"    aria-selected="true">Jugadores</a>
            </li>
        
          </ul>
        </div>


        <!-- LOADER -->
        <div class="container-fluid" v-if="loader">
            <div class="d-flex justify-content-center">
                <img src="dist/img/preloader.gif" class="img-fluid" alt="Responsive image">
            </div>
        </div>

        <?php 
            require_once"Controladores/php/ctrl_vistas.php";
        ?>
        
      
      
      
        <!-- FORMULARIO DE REGISTRO DE NUEVO JUGADOR -->
       



        <!-- JUEGA EL SUPER BINGO!!!!  AQUI ESTÁ LO DE LAS PARTIDAS-->
        <div class="container-fluid" v-show="Jugar">
          <div class="row">
            <div class="col-md-4">
                <br>
                <button class="m-10 mt-10 mb-10"  type="button" :class="['btn btn-info']">
                    <H1>CREAR NUEVA PARTIDA</H1>
                </button>
                <br><br>
                
                    <label for="inputAddress">NOMBRE DE LA PARTIDA</label>
                    <input type="text" v-model="NombreNuevaPartida" :class="['form-control', Alert_Nombre_Partida]" 
                    @click="Alert_Nombre_Partida = 'border-default'">
                    <br>
                    <button @click="RegistrarNuevaPartida()" type="button" class="btn btn-success mt-20 mb-50">Crear partida</button>
                    <br><br><br>
                
                
            </div>
            <div class="col-md-8">
                <div v-if="NUmpartidasExistentes > 0"  >
                    <br>
                    <H4 >TUS PARTIDAS</H4> <br>
                    <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                      <div v-for="(partida, index) in partidasQueSeMuestranDB">
                          <button style="margin-bottom: 2px;
                          margin-left: 2px;
                          margin-right: 2px;
                          margin-top: 2px;" type="button" :class="[partida.class_estilo]"
                          @click="CargarPartidaJuego(partida.id)">
                              <h2>{{partida.titulo}}</h2> 
                              <span class="badge badge-light">{{partida.estado}} - {{partida.fecha_registro}}</span>
                          </button>
                      </div>
                    </div>   
                </div>
            </div>

            <!-- LISTA DE JUGADAS -->
          
            
           
          </div>
          



        </div>







        <!-- JUEGA EL SUPER BINGO !!!   AQUÍ ESTÁ LA SECCIÓN CUANDO SE ABRE UNA PARTIDA EN PARTICULAR-->
         <!-- JUEGA EL SUPER BINGO!!!! -->
         <div class="container-fluid" v-show="JugandoPartida">
            <div class="row">
              <div class="col-md-3" v-for="PartidaSeleccionada in PartidaSeleccionada">
                  <br>
                  <button class="m-10 mt-10 mb-10"  type="button" :class="[PartidaSeleccionada.class_estilo]">
                      <H1>{{PartidaSeleccionada.titulo}}</H1>
                      <span class="badge badge-light">{{PartidaSeleccionada.estado}} - {{PartidaSeleccionada.fecha_registro}}</span>
                  </button>
                  <br><br>
                  <div class="d-flex justify-content-center" v-if="PartidaSeleccionada.estado == 'finalizada'">
                    <img src="dist/img/bingo-title.png" class="img-fluid" alt="Responsive image">
                  </div>
                     
                  <div v-if="PartidaSeleccionada.estado !== 'finalizada'">
                      <div class="form-row">
                          <h4>INGRESAR BALOTA</h4>
                            <div class="form-group col-md-6">
                              <label for="inputEmail4">LETRA </label>
                              <select v-model="LetraBalota" :class="['form-control', Alert_LetraBalota]" 
                              @click="Alert_LetraBalota = 'border-default'">
                                <option value="">----</option>
                                <option value="B">B</option>
                                <option value="I">I</option>
                                <option value="N">N</option>
                                <option value="G">G</option>
                                <option value="O">O</option>
                              </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="inputAddress">NÚMERO</label>
                                <input type="number" v-model="NumeroBalota" :class="['form-control', Alert_NumeroBalota]" 
                                @click="Alert_NumeroBalota = 'border-default'">
                            </div>
                        </div>
  
                        <button @click="IngresarBalota()" type="button" class="btn btn-success mt-20 mb-50">
                          ACEPTAR</button>
                  </div>
          


                      
                  <br><br><br>
                  
                  
              </div>
              <div class="col-md-6">
                  <div>
                      <br>
                      <H4 >BALOTAS JUGADAS</H4> <br>
                      
                      <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                        <div class="col-md-2" style="margin-bottom: -5000px;" v-for="(balota,index) in BalotasJugadasPartida">
                            <span style="cursor: pointer; "  @click="BalotaSeleccionada = BalotasJugadasPartida[index] , CargarDatosBalota()"
                              class="badge badge-pill badge-primary">
                              <h3>{{balota.balota_letra}}{{balota.balota_numero}}</h3>
                          </span>                           
                        </div>
                      </div>   
                  </div>
              </div>
  
              
              <!-- MOMENTOS DE LA JUGADA --> 
              <!-- aquí pondré datos como: numero de tablas en las que se encuentra la balota
              jugadores favorecidos -->
              <div class="col-md-3">
                  <br>
                  <div  v-if="LoaderInfoBalota" class="d-flex justify-content-center">
                    <img src="dist/img/preloader.gif" class="img-fluid" alt="Responsive image">
                  </div>
        
                  <div v-if="!LoaderInfoBalota">
                      <H5>Información balota {{BalotaSeleccionada.letra}}{{BalotaSeleccionada.numero}}</H5>
                      <div>
                        Jugadores: 
                        <p v-text="JugadoresRelacionadosBalota.length"></p>
                      </div>
                      <div>Tablas: 
                        <p v-text="TablasQueContienenAlaBalota_.length"></p> 
                      </div>

                      <div class="row" style="width: auto; height: 480px; overflow-y: scroll;">
                          <div v-for="jugador in JugadoresRelacionadosBalota" class="alert alert-primary" role="alert">
                              {{jugador.nombre}}
                              <a href="#" class="alert-link">Tabla: {{jugador.numero_tabla}}</a>. 
                              Direección: {{jugador.direccion}}
                              Fecha: registro: {{jugador.fecha_registro}}
                          </div>
                      </div>
                  </div>
                  

                  
              </div>
             
            </div>
            
  
  
  
          </div>









          <!-- ESTADISTÍCAS  --><!-- ESTADISTÍCAS  --><!-- ESTADISTÍCAS  -->
          <div class="container-fluid" v-show="Estadisticas">
              <div class="row">
                  <div class="col-md-3">
                      <button type="button" class="btn btn-danger">
                          <h4>Jugadores </h4> <span class="badge badge-light"><h4><?php echo count($jugadoresDB);?></h4></span>
                        </button>
                  </div>
              </div>
          </div>

        
    </div>
    





















    
    
    <script src="Controladores/js/Main.js"></script>

    

     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> 
  
    
    
    
  </body>
</html>