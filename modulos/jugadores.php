<?php ?>

<div class="container-fluid" id="Jugadores">
           <div class="row">
             <div class="col-md-4">
                <div v-if="AlertJugadorRegistrado" class="alert alert-primary" role="alert">
                    Proceso terminado. ¡Jugador registrado!
                </div>
    
                <div v-if="AlertNumTablaNoEncontrado" class="alert alert-danger" role="alert">
                    Error. Número de tabla no encontrado
                </div>
    
                <div v-if="AlertTablaTomada" class="alert alert-danger" role="alert">
                    Error. Tabla tomada
                </div>
    
                <div v-if="AlertJugadorExistente" class="alert alert-danger" role="alert">
                    Error. Este jugador ya se encuentra registrado.
                </div>
                <form>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputEmail4">Nombre</label>
                        <input v-model="nombre" type="text" :class="['form-control', ALert_nombre]" 
                        @click="ALert_nombre = 'border-default'">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputPassword4">Num. Cédula</label>
                        <input v-model="num_cedula" type="number" 
                        :class="['form-control', Alert_num_cedula]" 
                        @click="Alert_num_cedula = 'border-default'">
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-6">
                        <label for="inputAddress">Dirección</label>
                        <input v-model="direccion" type="text" :class="['form-control', Alert_direccion]" 
                        @click="Alert_direccion = 'border-default'">
                      </div>
                      <div class="form-group col-md-6">
                        <label for="inputAddress2">Teléfono</label>
                        <input v-model="telefono" type="number" :class="['form-control', Alert_telefono]" 
                        @click="Alert_telefono = 'border-default'">
                      </div>
                    </div>
    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                          <label for="inputAddress">Número de la tabla</label>
                          <input type="text" v-model="numero_tabla" :class="['form-control', Alert_numero_tabla]" 
                          @click="Alert_numero_tabla = 'border-default'">
                        </div>
                    </div>
  
                    <br><br>
                    <button @click="RegistrarJugador()" type="button" class="btn btn-success mt-20 mb-50">Registrar</button>
                    <br><br><br>
                </form>
             </div>

             <div class="col-md-8"  >

                <div class="row">
                    <div class="col-md-6">
                      <input @keyup="BuscarCedula()" v-model="buscar" type="text"
                      class="form-control" placeholder="Buscar por número de cédula" >
                    </div>
                    <div class="col-md-6">
                      <input @keyup="BuscarTabla()" v-model="buscarT" type="text"
                      class="form-control" placeholder="Buscar por número de tabla" >
                    </div>
                </div>
                  
                <!-- JUGADORES TABLA -->
                <div style="width: auto; height: 320px; overflow-y: scroll;">
                    <table class="table table-responsive table-hover" id="jugadores">
                      <thead>
                        <tr>
                          <th scope="col">CC</th>
                          <th scope="col">NOMBRE</th>
                          <th scope="col">TELEFONO</th>
                          <th scope="col">DIRECCIÓN</th>
                          <th scope="col">TABLA</th>
                          <th scope="col">REGISTRO</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr style="cursor: pointer;" v-for="(jugador, index) in JugadoresQueSeMuestranDB">
                          <td>{{jugador.num_identificacion}}</td>
                          <td>{{jugador.nombre}}</td>
                          <td>{{jugador.telefono}}</td>
                          <td>{{jugador.direccion}}</td>
                          <td>{{jugador.numero_tabla}}</td>
                          <td>{{jugador.fecha_registro}}</td>
                        </tr>
                      </tbody>
                    </table>
                    <!-- JUGADORES TABLA -->

                        
                        

                </div>
              </div>
           </div>
        </div>

        <script src="Controladores/js/Jugador.js"></script>
