<?php   

require_once "mdlJugadores.php";

$Jugadores = mdlJugadores::CargarJugadores(null, null, "DESC", "id");
$cedulas = array();
foreach ($Jugadores as $key => $jugador) {
    array_push($cedulas,$jugador["num_identificacion"] );
}

// unifico todo : 
$cedulas_sin_repetir = array_unique($cedulas);

$stmt = Conection::conectar()->prepare("TRUNCATE TABLE jugadores");
$stmt->execute();


foreach($cedulas_sin_repetir as $cedula){
    foreach($Jugadores as $jugador){
        if ($jugador["num_identificacion"] == $cedula) {
            $datos = array(
                "nombre" => $jugador["nombre"],
                "num_cedula" => $jugador["num_identificacion"],
                "direccion" => $jugador["direccion"],
                "telefono" => $jugador["telefono"],
                "numero_tabla" => $jugador["numero_tabla"],
                "fecha_registro" => $jugador["fecha_registro"]
            );
            $registrar = mdlJugadores::RegistrarJugador($datos);
        break;
        }
    }
}