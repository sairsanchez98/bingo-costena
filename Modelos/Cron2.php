<?php
   require_once "conexion.php";
   require_once "mdlPartidas.php";
   require_once "mdlJugadores.php";
   require_once "tablas.php";

////////////// VAMOS A  VALIDAR EL BINGO Y SI HAY BINGO LO ANOTAREMOS
 $partidas = mdlPartidas::CargarPartidas(null, null, "DESC", "id");
 
 foreach($partidas as $partida){
    $tablas_activas = array();
     $BalotasPartidas = mdlPartidas::CargarJugadasPartidas_("id_partida", $partida["id"],null , null);
     foreach($BalotasPartidas as $balota){
         if($balota["balota_letra"] == "B") {
             foreach($tablas as $tabla){
                 if ($tabla["b"] == $balota["balota_numero"] || $tabla["bb"] == $balota["balota_numero"] 
                     || $tabla["bbb"] == $balota["balota_numero"] || $tabla["bbbb"] == $balota["balota_numero"] ||
                     $tabla["bbbbb"] == $balota["balota_numero"] 
                 ) { 
                     array_push($tablas_activas, $tabla["codigo_tabla"]);
                 }
             }
         }

         else if($balota["balota_letra"] == "I") {
             foreach($tablas as $tabla){
                 if ($tabla["i"] == $balota["balota_numero"] || $tabla["ii"] == $balota["balota_numero"] 
                     || $tabla["iii"] == $balota["balota_numero"] || $tabla["iiii"] == $balota["balota_numero"] ||
                     $tabla["iiiii"] == $balota["balota_numero"] 
                 ) { 
                     array_push($tablas_activas, $tabla["codigo_tabla"]);
                     
                 }
             }
         }

         else if($balota["balota_letra"] == "N") {
             foreach($tablas as $tabla){
                 if ($tabla["n"] == $balota["balota_numero"] || $tabla["nn"] == $balota["balota_numero"] 
                     || $tabla["nnnn"] == $balota["balota_numero"] || $tabla["nnnnn"] == $balota["balota_numero"] 
                 ) { 
                     array_push($tablas_activas, $tabla["codigo_tabla"]);
                 
                 }
             }
         }


         else if($balota["balota_letra"] == "G") {
             foreach($tablas as $tabla){
                 if ($tabla["g"] == $balota["balota_numero"] || $tabla["gg"] == $balota["balota_numero"] 
                     || $tabla["ggg"] == $balota["balota_numero"] || $tabla["gggg"] == $balota["balota_numero"] ||
                     $tabla["ggggg"] == $balota["balota_numero"] 
                 ) { 
                     array_push($tablas_activas, $tabla["codigo_tabla"]);
                 
                     
                 }
             }
         }


         else if($balota["balota_letra"] == "O") {
             foreach($tablas as $tabla){
                 if ($tabla["o"] == $balota["balota_numero"] || $tabla["oo"] == $balota["balota_numero"] 
                     || $tabla["ooo"] == $balota["balota_numero"] || $tabla["oooo"] == $balota["balota_numero"] ||
                     $tabla["ooooo"] == $balota["balota_numero"] 
                 ) { 
                     array_push($tablas_activas, $tabla["codigo_tabla"]);

                 
                 }
             }
         }


         //var_dump($tablas_activas);


         foreach ($tablas as  $tb_DB) {
             $contador_veces_cantada = 0;
             foreach($tablas_activas as $ta){
                if ($tb_DB["codigo_tabla"] == $ta) {
                    $contador_veces_cantada = $contador_veces_cantada + 1 ;
                }else{
                    
                }
             }

             if ($contador_veces_cantada >= 24) {
                 echo  "BINGO !!";
                $cambiarEstadoPartida = mdlPartidas::CambiarEstadoPartida($partida["id"], "finalizada");
            
                $stmt = Conection::conectar()->prepare("INSERT INTO 
                bingo(id_partida, tabla) 
                VALUES (:id_partida , :tabla)");
                $stmt->bindParam(":id_partida",$partida["id"], PDO::PARAM_INT);
                $stmt->bindParam(":tabla", $tb_DB["codigo_tabla"], PDO::PARAM_STR);
                
                
                $stmt->execute();
                break;
             }else{
                 
             }
         }

     }
 }
