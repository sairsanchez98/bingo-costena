<?php

## este archivo lo usaré para subir información de jugadores a la DB a través de un array externo 
## es decir, que no se subiran manualmente a través de la plataforma porque dichos jugadores 
## ya estaban registrados en un archivo de excel

## empezamos ... 

## tomamos al array que vamos a meter a la tabla de jugadores: 
$jugadores = array(); // hay que llenarlo si está vacío




  



  ## una vez tenga instanciado el array procedo a rrecorrerlo e insertar cada jugador a la DB
require_once"conexion.php" ; ## me conecto a la DB
require_once "mdlJugadores.php";
require_once "../ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();


  foreach ($jugadores as $key => $jugador) {
    $consultar_jugador = mdlJugadores::CargarJugadores("num_identificacion", $jugador["cedula"], "DESC", "id");
    if (!$consultar_jugador) { ## si no encuentra el jugador quiere decir que no existe y por eso hay que registrarlo
      $datos = array(
        "nombre" => $jugador["nombre"]. " ". $jugador["apellido"],
        "num_cedula" => $jugador["cedula"],
        "direccion" => $jugador["barrio"],
        "telefono" => $jugador["telefono"],
        "numero_tabla" => $jugador["tabla"],
        "fecha_registro" => $fechaActual
      );
      $registrar = mdlJugadores::RegistrarJugador($datos);
      echo "registrado";

    }else{
      echo "ya existe";
    }
    
  }