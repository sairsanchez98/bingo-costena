<?php
    
    

    require_once"Modelos/tablas.php";

    if (@$_GET["lote_tabla"] == 1) {
        $tablas = $tablas_lote1;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote1.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote1.json";
    }else if (@$_GET["lote_tabla"] == 2) {
        $tablas = $tablas_lote2;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote2.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote2.json";
    }else if (@$_GET["lote_tabla"] == 3) {
        $tablas = $tablas_lote3;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote3.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote3.json";
    }else if (@$_GET["lote_tabla"] == 4) {
        $tablas = $tablas_lote4;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote4.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote4.json";
    }else if (@$_GET["lote_tabla"] == 5) {
        $tablas = $tablas_lote5;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote5.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote5.json";
    }else if (@$_GET["lote_tabla"] == 6) {
        $tablas = $tablas_lote6;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote6.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote6.json";
    }else if (@$_GET["lote_tabla"] == 7) {
        $tablas = $tablas_lote7;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote7.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote7.json";
    }else if (@$_GET["lote_tabla"] == 8) {
        $tablas = $tablas_lote8;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote8.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote8.json";
    }else if (@$_GET["lote_tabla"] == 9) {
        $tablas = $tablas_lote9;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote9.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote9.json";
    }else if (@$_GET["lote_tabla"] == 10) {
        $tablas = $tablas_lote10;
        $sair_balotas_analizadas = "Modelos/DB/DB_balotas_analizadas_lote10.json";
        $sair_tablas_cantadas = "Modelos/DB/DB_tablas_cantadas_lote10.json";
    }

    
    //$partida = mdlPartidas::CargarPartidas("id", $_GET["id_partida"], "DESC", "id");

    //$tablasCantadasporPartida = mdlPartidas::CargarTablasCantadas("id_partida", $_GET["id_partida"], "DESC", "veces_cantada");

    ## traigo a las partidas 
    $data = file_get_contents("Modelos/DB/DB_partidas.json");
    $partidas = json_decode($data, true);

    foreach($partidas as $partida){

        ## por cada partida me trago a sus balotas... 
        $data = file_get_contents("Modelos/DB/DB_balotas_partidas.json");
        $balotas_partidas = json_decode($data, true);

        ## recorro las balotas para preguntar cual es de esta partida:
        $balotasBD = array(); ## auí se alacenan las balotas por partida
        foreach ($balotas_partidas as $balota){
            if ($balota["id_partida"] == $partida["id"]) {
                array_push($balotasBD , $balota);
            }
        }

        ## ahora recoro cada balota de esta partida: 
        foreach ($balotasBD as $key => $balota) {
            $tablas_activas = array();

            ## voy a traer la tabla de tablas cantadas para ver si esta balota ya fue
            ## cantada junto con su tabla ... porque no la podemos repetir
            $data = file_get_contents($sair_balotas_analizadas);
            $balotas_analizadas = json_decode($data, true);
           
           if ( count($balotas_analizadas)  == 0 ) {
               //echo "no se han analizado balotas<br>";
                 ## hago el registro de que la balota fue analizada 
                 $balota_analizada = array(
                    "id_partida" => $partida["id"],
                    "balota_letra" =>  $balota["letra"],
                    "balota_numero" => $balota["numero"]
                );
                array_push($balotas_analizadas , $balota_analizada);
                $jsonencoded = json_encode($balotas_analizadas,JSON_UNESCAPED_UNICODE);
                $registrar = fopen($sair_balotas_analizadas, 'w');
                fwrite($registrar, $jsonencoded);
                fclose($registrar); 


                ##ahora reviso en que tabla está dicha balora 
                ## para luego hacer el registro de las balotas encontradas
                if($balota["letra"] == "B") {
                    foreach($tablas as $tabla){
                        if ($tabla["b"] == $balota["numero"] || $tabla["bb"] == $balota["numero"] 
                            || $tabla["bbb"] == $balota["numero"] || $tabla["bbbb"] == $balota["numero"] ||
                            $tabla["bbbbb"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                } else if($balota["letra"] == "I") {
                    foreach($tablas as $tabla){
                        if ($tabla["i"] == $balota["numero"] || $tabla["ii"] == $balota["numero"] 
                            || $tabla["iii"] == $balota["numero"] || $tabla["iiii"] == $balota["numero"] ||
                            $tabla["iiiii"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }else if($balota["letra"] == "N") {
                    foreach($tablas as $tabla){
                        if ($tabla["n"] == $balota["numero"] || $tabla["nn"] == $balota["numero"] 
                            || $tabla["nnnn"] == $balota["numero"] || $tabla["nnnnn"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }else if($balota["letra"] == "G") {
                    foreach($tablas as $tabla){
                        if ($tabla["g"] == $balota["numero"] || $tabla["gg"] == $balota["numero"] 
                            || $tabla["ggg"] == $balota["numero"] || $tabla["gggg"] == $balota["numero"] ||
                            $tabla["ggggg"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }
                else if($balota["letra"] == "O") {
                    foreach($tablas as $tabla){
                        if ($tabla["o"] == $balota["numero"] || $tabla["oo"] == $balota["numero"] 
                            || $tabla["ooo"] == $balota["numero"] || $tabla["oooo"] == $balota["numero"] ||
                            $tabla["ooooo"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }
                 ## ahora ingresamos las tablas cantadas a la db de tablas cantadas
                 $data = file_get_contents($sair_tablas_cantadas);
                 $tablas_cantadas = json_decode($data, true);
                 foreach ($tablas_activas as $key => $tablac) {
                     array_push($tablas_cantadas , $tablac);
                 }
                 $jsonencoded = json_encode($tablas_cantadas,JSON_UNESCAPED_UNICODE);
                 $registrar = fopen($sair_tablas_cantadas, 'w');
                 fwrite($registrar, $jsonencoded);
                 fclose($registrar); 


           }else{
               //echo "analisis de balota <br>";
               $balota_analizada = false;
            foreach ($balotas_analizadas as $key => $tblc) {
                ### debo verificar si la balota no está en la db 
                ## puede estar ... pero no puede estar y estar asociada a la partida en cuestión 
                ## eso sería un error :: ::
                if ( $balota["letra"]  == $tblc["balota_letra"] &&
                     $balota["numero"] == $tblc["balota_numero"]  ) 
                {
                   $balota_analizada = true;
                    break;
                }
            }

            if (!$balota_analizada) { // si la balota en cuestión NO ha sido analizada
                 ## hago el registro de que la balota fue analizada 
                 $balota_analizada = array(
                    "id_partida" => $partida["id"],
                    "balota_letra" =>  $balota["letra"],
                    "balota_numero" => $balota["numero"]
                );
                array_push($balotas_analizadas , $balota_analizada);
                $jsonencoded = json_encode($balotas_analizadas,JSON_UNESCAPED_UNICODE);
                $registrar = fopen($sair_balotas_analizadas, 'w');
                fwrite($registrar, $jsonencoded);
                fclose($registrar); 


                ##ahora reviso en que tabla está dicha balora 
                ## para luego hacer el registro de las balotas encontradas
                if($balota["letra"] == "B") {
                    foreach($tablas as $tabla){
                        if ($tabla["b"] == $balota["numero"] || $tabla["bb"] == $balota["numero"] 
                            || $tabla["bbb"] == $balota["numero"] || $tabla["bbbb"] == $balota["numero"] ||
                            $tabla["bbbbb"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                } else if($balota["letra"] == "I") {
                    foreach($tablas as $tabla){
                        if ($tabla["i"] == $balota["numero"] || $tabla["ii"] == $balota["numero"] 
                            || $tabla["iii"] == $balota["numero"] || $tabla["iiii"] == $balota["numero"] ||
                            $tabla["iiiii"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }else if($balota["letra"] == "N") {
                    foreach($tablas as $tabla){
                        if ($tabla["n"] == $balota["numero"] || $tabla["nn"] == $balota["numero"] 
                            || $tabla["nnnn"] == $balota["numero"] || $tabla["nnnnn"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }else if($balota["letra"] == "G") {
                    foreach($tablas as $tabla){
                        if ($tabla["g"] == $balota["numero"] || $tabla["gg"] == $balota["numero"] 
                            || $tabla["ggg"] == $balota["numero"] || $tabla["gggg"] == $balota["numero"] ||
                            $tabla["ggggg"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }
                else if($balota["letra"] == "O") {
                    foreach($tablas as $tabla){
                        if ($tabla["o"] == $balota["numero"] || $tabla["oo"] == $balota["numero"] 
                            || $tabla["ooo"] == $balota["numero"] || $tabla["oooo"] == $balota["numero"] ||
                            $tabla["ooooo"] == $balota["numero"] 
                        ) { 
                            array_push($tablas_activas, $tabla["codigo_tabla"]);
                        }   
                    }
                }


                ## ahora ingresamos las tablas cantadas a la db de tablas cantadas
                $data = file_get_contents($sair_tablas_cantadas);
                $tablas_cantadas = json_decode($data, true);
                foreach ($tablas_activas as $key => $tablac) {
                    array_push($tablas_cantadas , $tablac);
                }
                $jsonencoded = json_encode($tablas_cantadas,JSON_UNESCAPED_UNICODE);
                $registrar = fopen($sair_tablas_cantadas, 'w');
                fwrite($registrar, $jsonencoded);
                fclose($registrar); 
            }

           }
           

        }

    }



    
    $data = file_get_contents($sair_tablas_cantadas);
    $tablas_cantadas = json_decode($data, true);
    $tablas_numero_veces_cantadas = array();
    foreach ($tablas_cantadas as $key => $tb_can) {
        $cont_de_veces_cantada = 0;
        foreach ($tablas_cantadas as $key => $tb_can2) {
            if ($tb_can == $tb_can2) {
                $cont_de_veces_cantada = $cont_de_veces_cantada +1;
            }
        }
        $tabla_veces = array(
            "veces" => $cont_de_veces_cantada,
            "tabla" =>  $tb_can
        );
        array_push($tablas_numero_veces_cantadas , $tabla_veces);
    }
    rsort($tablas_numero_veces_cantadas, SORT_REGULAR);
    //array_unique($tablas_numero_veces_cantadas);
    

    ##var_dump($tablas_numero_veces_cantadas);





   
?>