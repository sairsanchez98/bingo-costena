

var bingo = new Vue({
    el: "#Bingo",
    data: {
        // BASES DE DATOS : :  : 
        tablas_bingo_DB: [],
        JugadoresDB: [],
        JugadoresQueSeMuestranDB: [],
        partidasDB: [],
        NUmpartidasExistentes: "",
        partidasQueSeMuestranDB: [],

        PartidaSeleccionada: [],
        BalotasJugadasPartida: [],
        BalotaSeleccionada: [], // aquí se almacena la info de la balota que se acaba de jugar o la que sea seleccionada por el usuario admin
        JugadoresRelacionadosBalota: [], // y aqui se van a almacenar los jugadores que tienen esa balota
        TablasQueContienenAlaBalota_ : [], //y aqui se almacenan las tablas que contienen a esa balota


        ///DATOS NUEVA PARTIDA: 
        NombreNuevaPartida: "",
        Alert_Nombre_Partida : "border-default",


        ///// INGRESAR BALOTA:::
        existeBalota: false,
        NumeroBalota: "",
        LetraBalota: "",
        Alert_NumeroBalota: "border-default",
        Alert_LetraBalota: "border-default",
        DatosBalota: [], // aquí se almacenará la información de una balota cuando sea requrido



        ///DATOS PARA REGISTRAR JUGADOR : :: 
        nombre: "",
        num_cedula: "",
        telefono: "",
        direccion: "",
        numero_tabla: "",

        
        // alertas
        ALert_nombre: "border-default",
        Alert_num_cedula: "border-default",
        Alert_telefono: "border-default",
        Alert_direccion: "border-default",
        Alert_numero_tabla: "border-default",
        AlertJugadorRegistrado: false,
        AlertNumTablaNoEncontrado: false,
        AlertTablaTomada: false,
        AlertJugadorExistente: false,


        ///// COMPONENTES:
        Estadisticas: true,
        Jugar: false,
        JugandoPartida: false,
        Jugadores : false,
        Validacion : false,
        loader: false,
        LoaderInfoBalota : false,
        


        /// BUSQUEDA DE JUGADORES:
        buscar : "",
        jugadoresEncontrados: [],


        ////ESTADIÍSTICAs
        totalNumJugadores: ""



    },
    methods: {

        cargarTablas:function(){
            
            axios.get("Controladores/php/ctrlTablas.php?CargarTablas=CargarTodo").then(function(response){
                let datos = response.data;
                bingo.tablas_bingo_DB = datos.tablas_bingo;
                console.log(response);
            })
            
        },

        CargarPartidas: function(){
            bingo.HabilitarComponente("Loader");
            axios.get("Controladores/php/ctrlPartidas.php?CargarPartidas=CargarTodo")
            .then(function(response){
                let datos = response.data;
                bingo.partidasDB = datos.respuesta;
                bingo.partidasQueSeMuestranDB = datos.respuesta;
                bingo.HabilitarComponente("JugarSinCargarPartidas");
                bingo.NUmpartidasExistentes = datos.respuesta.length;
                console.log(response);
            })
        },

        // esta función es para que no se habilite el componente de PARTIDAS sino que solo se recarguen las mismas
        ReCargarPartidas: function(){ 
            axios.get("Controladores/php/ctrlPartidas.php?CargarPartidas=CargarTodo")
            .then(function(response){
                let datos = response.data;
                bingo.partidasDB = datos.respuesta;
                bingo.partidasQueSeMuestranDB = datos.respuesta;
            })
        },

       
        CargarPartidaJuego: function(id){

            // recarga la partida solamente: 
            this.HabilitarComponente("Loader");
            axios.get("Controladores/php/ctrlPartidas.php?CargarPartidas=CargarPartida&id_partida="+id)
            .then(function(response){
                let datos = response.data;
                  
                if(datos.respuesta){
                    bingo.PartidaSeleccionada = datos.respuesta;
                    console.log(response);
                    bingo.HabilitarComponente("JugandoPartida");
                }             
                
            })
             // ahora traigo las balotas y demás datos de la partida
             axios.get("Controladores/php/ctrlPartidas.php?CargarJugadasPartidas_&id_partida="+id)
             .then(function(response){
                 let datos = response.data;
                 
                    bingo.BalotasJugadasPartida = datos.balotas;
                    //bingo.BalotaSeleccionada = datos.BalotasFull[0];
                    //bingo.JugadoresRelacionadosBalota  = datos.jugadores;
                    console.log(response);
                    bingo.HabilitarComponente("JugandoPartida");
                 
                 
             })
        },


        CargarDatosBalota: function (){
            
            
            this.TablasQueContienenAlaBalota(
                this.BalotaSeleccionada["balota_letra"],
                this.BalotaSeleccionada["balota_numero"]
            );
            

            // enveremos
            this.DatosBalota = [
                {num_tablas : this.TablasQueContienenAlaBalota.length } ,
                {num_jugadores : this.JugadoresRelacionadosBalota.length }
            ]

            // axios.get("Controladores/php/ctrlPartidas.php?cargar_jugadores_de_una_balota=ok&letra="+
            // this.BalotaSeleccionada.letra+"&numero="+this.BalotaSeleccionada.numero+"&id_partida="+
            // this.PartidaSeleccionada[0]["id"])
            // .then(function(response){
            //     let datos = response.data;
            //     bingo.JugadoresRelacionadosBalota = datos.jugadores;
            //     console.log(response);
            // })
        },


        TablasQueContienenAlaBalota(letra , numero ){
            
            if(letra == "B"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["b"] == numero || this.tablas_bingo_DB[i]["bb"] == numero
                     || this.tablas_bingo_DB[i]["bbb"] == numero || this.tablas_bingo_DB[i]["bbbb"] == numero
                     || this.tablas_bingo_DB[i]["bbbbb"] == numero){
                        bingo.TablasQueContienenAlaBalota_.push(this.tablas_bingo_DB[i]["codigo_tabla"]); 
                        
                        // traigo a los jugadores que tienen la tabla encontrada 
                        for (var j = 0; j < this.JugadoresDB.length; j++) {
                            if(this.JugadoresDB[j]["numero_tabla"] == this.tablas_bingo_DB[i]["codigo_tabla"])
                            {
                                this.JugadoresRelacionadosBalota.push(this.JugadoresDB[j]);
                            }
                        }
                    }
                }
            }

            else if(letra == "I"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["i"] == numero || this.tablas_bingo_DB[i]["ii"] == numero
                     || this.tablas_bingo_DB[i]["iii"] == numero || this.tablas_bingo_DB[i]["iiii"] == numero
                     || this.tablas_bingo_DB[i]["iiiii"] == numero){
                        bingo.TablasQueContienenAlaBalota_.push(this.tablas_bingo_DB[i]["codigo_tabla"]); 
                        // traigo a los jugadores que tienen la tabla encontrada 
                        for (var j = 0; j < this.JugadoresDB.length; j++) {
                            if(this.JugadoresDB[j]["numero_tabla"] == this.tablas_bingo_DB[i]["codigo_tabla"])
                            {
                                this.JugadoresRelacionadosBalota.push(this.JugadoresDB[j]);
                            }
                        }
                    }
                }
            }

            else if(letra == "N"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["n"] == numero || this.tablas_bingo_DB[i]["nn"] == numero
                      || this.tablas_bingo_DB[i]["nnnn"] == numero || this.tablas_bingo_DB[i]["nnnnn"] == numero){
                        bingo.TablasQueContienenAlaBalota_.push(this.tablas_bingo_DB[i]["codigo_tabla"]); 
                        // traigo a los jugadores que tienen la tabla encontrada 
                        for (var j = 0; j < this.JugadoresDB.length; j++) {
                            if(this.JugadoresDB[j]["numero_tabla"] == this.tablas_bingo_DB[i]["codigo_tabla"])
                            {
                                this.JugadoresRelacionadosBalota.push(this.JugadoresDB[j]);
                            }
                        }
                    }
                }
            }

            else if(letra == "G"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["g"] == numero || this.tablas_bingo_DB[i]["gg"] == numero
                     || this.tablas_bingo_DB[i]["ggg"] == numero || this.tablas_bingo_DB[i]["gggg"] == numero
                     || this.tablas_bingo_DB[i]["ggggg"] == numero){
                        bingo.TablasQueContienenAlaBalota_.push(this.tablas_bingo_DB[i]["codigo_tabla"]); 
                        
                        // traigo a los jugadores que tienen la tabla encontrada 
                        for (var j = 0; j < this.JugadoresDB.length; j++) {
                            if(this.JugadoresDB[j]["numero_tabla"] == this.tablas_bingo_DB[i]["codigo_tabla"])
                            {
                                this.JugadoresRelacionadosBalota.push(this.JugadoresDB[j]);
                            }
                        }
                    }
                }
            }

            else if(letra == "O"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["o"] == numero || this.tablas_bingo_DB[i]["oo"] == numero
                     || this.tablas_bingo_DB[i]["ooo"] == numero || this.tablas_bingo_DB[i]["oooo"] == numero
                     || this.tablas_bingo_DB[i]["ooooo"] == numero){
                        bingo.TablasQueContienenAlaBalota_.push(this.tablas_bingo_DB[i]["codigo_tabla"]); 
                        // traigo a los jugadores que tienen la tabla encontrada 
                        for (var j = 0; j < this.JugadoresDB.length; j++) {
                            if(this.JugadoresDB[j]["numero_tabla"] == this.tablas_bingo_DB[i]["codigo_tabla"])
                            {
                                this.JugadoresRelacionadosBalota.push(this.JugadoresDB[j]);
                            }
                        }
                    }
                }
            }

            
            
        },


        ValidarExistenciaDeBalota: function (letra , numero ){
            console.log(letra+numero);
            if(letra == "B"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["b"] == numero || this.tablas_bingo_DB[i]["bb"] == numero
                     || this.tablas_bingo_DB[i]["bbb"] == numero || this.tablas_bingo_DB[i]["bbbb"] == numero
                     || this.tablas_bingo_DB[i]["bbbbb"] == numero){
                        bingo.existeBalota = true;
                        break;
                    }else{
                        return false;
                    }
                }
            }

            else if(letra == "I"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["i"] == numero || this.tablas_bingo_DB[i]["ii"] == numero
                     || this.tablas_bingo_DB[i]["iii"] == numero || this.tablas_bingo_DB[i]["iiii"] == numero
                     || this.tablas_bingo_DB[i]["iiiii"] == numero){
                        bingo.existeBalota = true;
                        break;
                    }else{
                        return false;
                    }
                }
            }

            else if(letra == "N"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["n"] == numero || this.tablas_bingo_DB[i]["nn"] == numero
                      || this.tablas_bingo_DB[i]["nnnn"] == numero || this.tablas_bingo_DB[i]["nnnnn"] == numero){
                        bingo.existeBalota = true;
                        break;
                    }else{
                        return false;
                    }
                }
            }

            else if(letra == "G"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["g"] == numero || this.tablas_bingo_DB[i]["gg"] == numero
                     || this.tablas_bingo_DB[i]["ggg"] == numero || this.tablas_bingo_DB[i]["gggg"] == numero
                     || this.tablas_bingo_DB[i]["ggggg"] == numero){
                        bingo.existeBalota = true;
                        break;
                    }else{
                        return false;
                    }
                }
            }

            else if(letra == "O"){
                for (var i = 0; i < this.tablas_bingo_DB.length; i++) {
                    if(this.tablas_bingo_DB[i]["O"] == numero || this.tablas_bingo_DB[i]["OO"] == numero
                     || this.tablas_bingo_DB[i]["OOO"] == numero || this.tablas_bingo_DB[i]["OOOO"] == numero
                     || this.tablas_bingo_DB[i]["OOOOO"] == numero){
                        bingo.existeBalota = true;
                        break;
                    }else{
                        return false;
                    }
                }
            }

        },


       

        // jugando :: :
        IngresarBalota: function(){
            this.HabilitarComponente("Loader")
            //this.ValidarExistenciaDeBalota(this.LetraBalota , this.NumeroBalota);
            
           if(this.LetraBalota !== "" && this.NumeroBalota !== "" ){
            formdata = new FormData();
            formdata.append("ingresar_balota", "ok");
            formdata.append("letra", this.LetraBalota);
            formdata.append("numero", this.NumeroBalota);
            formdata.append("id_partida", this.PartidaSeleccionada[0]["id"]);
            // envio el estado para que php lo verifique si el estado es "creada" entonces deberá pasar
            // a "jugando"
            formdata.append("estado_partida", this.PartidaSeleccionada[0]["estado"])

            axios.post("Controladores/php/ctrlPartidas.php",formdata)
            .then(function(response){
                let datos  = response.data;
                console.log(response);

                
                bingo.existeBalota = false;
                bingo.LetraBalota = "";
                bingo.NumeroBalota = "";
                bingo.HabilitarComponente("JugandoPartida");
                bingo.ReCargarPartidas();
                bingo.CargarPartidaJuego(datos.id_partida);
            })
           }else{
            bingo.MostrarAlertas("IngresarBalota", "CamposVacios");
            bingo.HabilitarComponente("JugandoPartida");
           }
        },


        RegistrarJugador: function(){
                this.HabilitarComponente("Loader");
                this.AlertJugadorRegistrado =  false;
                this.AlertNumTablaNoEncontrado =  false;
                this.AlertTablaTomada = false;
                this.AlertJugadorExistente =  false;

            if(this.nombre !== "" && this.num_cedula !== "" && this.direccion !== "" &&
                this.telefono !== "" && this.numero_tabla !== ""){
                    formdata = new FormData();
                    formdata.append("registrarJugador", "ok");
                    formdata.append("nombre", this.nombre);
                    formdata.append("num_cedula", this.num_cedula);
                    formdata.append("direccion", this.direccion);
                    formdata.append("telefono", this.telefono);
                    formdata.append("numero_tabla", this.numero_tabla);


                    axios.post("Controladores/php/ctrlJugadores.php", formdata).then(function(response){
                        let datos = response.data;
                        console.log(response);

                        if(datos.respuesta == "ok"){
                            bingo.MostrarAlertas("registrarJugador", "ok");
                            // formateamos
                            bingo.nombre = "";
                            bingo.num_cedula = "";
                            bingo.telefono =  "";
                            bingo.direccion = "";
                            bingo.numero_tabla = "";
                            bingo.HabilitarComponente("Jugadores");
                            bingo.CargarJugadores();

                        }else if(datos.respuesta == "NumTablaNoEncontrado"){
                            bingo.MostrarAlertas("registrarJugador", "NumTablaNoEncontrado");
                            bingo.HabilitarComponente("Jugadores");
                        }else if(datos.respuesta == "NumTablaTomado"){
                            bingo.MostrarAlertas("registrarJugador", "NumTablaTomado");
                            bingo.HabilitarComponente("Jugadores");
                        }else if(datos.respuesta == "JugadorExistente"){
                            bingo.MostrarAlertas("registrarJugador", "JugadorExistente");
                            bingo.HabilitarComponente("Jugadores");
                        }
                    })
                }else{
                    this.MostrarAlertas("registrarJugador", "camposVacios");
                    bingo.HabilitarComponente("Jugadores");
                }
        },

        RegistrarNuevaPartida : function (){
            this.HabilitarComponente("Loader");
            formdata = new FormData();
            formdata.append("crearPartida", "ok");
            formdata.append("titulo", this.NombreNuevaPartida);

            axios.post("Controladores/php/ctrlPartidas.php", formdata).then(function(response){
                let datos =  response.data;
                if(datos.respuesta == "ok"){
                    bingo.MostrarAlertas("CrearPartida", "ok");   
                    bingo.HabilitarComponente("Jugar");
                    bingo.NombreNuevaPartida = "";
                }
                console.log(response);
            })
        },



        CargarJugadores: function(){
            this.HabilitarComponente("Loader");
            axios.get("Controladores/php/ctrlJugadores.php?CargarJugadores=CargarTodo")
            .then(function(response){
                let datos = response.data;
                bingo.JugadoresDB = datos.respuesta;
                bingo.JugadoresQueSeMuestranDB = datos.respuesta;
                bingo.HabilitarComponente("JugadoresSinLoader");
                console.log(response);
            })
        },

        RECargarJugadores: function(){
            axios.get("Controladores/php/ctrlJugadores.php?CargarJugadores=CargarTodo")
            .then(function(response){
                let datos = response.data;
                bingo.JugadoresDB = datos.respuesta;
                bingo.JugadoresQueSeMuestranDB = datos.respuesta;
                console.log(response);
                bingo.totalNumJugadores  = bingo.JugadoresDB.length;
            })
        },



        Buscar: function()
        {
                if(this.buscar == "")
                {
                this.CargarJugadores();
                //console.log("vacio");
                }else{
                //convertimos el texto que está ingresando 
                //el usuario a minusculas para que sea más fácil realizar el filtro
                const texto = bingo.buscar.toLowerCase();
                this.jugadoresEncontrados.splice(0);
                
                for(let _jugador_ of this.JugadoresDB )
                    {
                        let _jugador = _jugador_["num_identificacion"];
                        let _infoJugador = _jugador.toLowerCase();
                        //console.log(_infoJugador);
                    
                        if(_infoJugador.indexOf(this.buscar) !== -1){
                                this.jugadoresEncontrados.push(_jugador_);
                                this.JugadoresQueSeMuestranDB = this.jugadoresEncontrados;
                        }
                    }
                }
                    
        },

        CargarEstadisticas: function (){
            
            this.RECargarJugadores();
                
            
                
            
            
        },

        HabilitarComponente: function (componente){
            switch(componente){
                case "Estadisticas": 
                    this.Estadisticas = true;
                    this.Jugar = false;
                    this.Jugadores = false;
                    this.Validacion = false;
                    this.loader = false;
                    this.JugandoPartida = false;
                    this.CargarEstadisticas();
                break;

                case "Jugar":
                    this.Estadisticas = false;
                    this.JugandoPartida = false;
                    this.Jugar = true;
                    this.Jugadores = false;
                    this.Validacion = false;
                    this.loader = false;
                    this.CargarPartidas();
                break;

                case "JugandoPartida":
                    this.Estadisticas = false;
                    this.Jugar = false;
                    this.JugandoPartida = true;
                    this.Jugadores = false;
                    this.Validacion = false;
                    this.loader = false;
                break;
                
                case "JugarSinCargarPartidas": 
                    this.Estadisticas = false;
                    this.Jugar = true;
                    this.Jugadores = false;
                    this.Validacion = false;
                    this.loader = false;
                    this.JugandoPartida = false;
                break;

                

                case "Jugadores": 
                    this.Estadisticas = false;
                    this.Jugar = false;
                    this.Jugadores = true;
                    this.Validacion = false;
                    this.loader = false;
                    this.JugandoPartida = false;
                    this.CargarJugadores();
                break;

                case "JugadoresSinLoader":
                    this.JugandoPartida = false;
                    this.Estadisticas = false;
                    this.Jugar = false;
                    this.Jugadores = true;
                    this.Validacion = false;
                    this.loader = false;
                break;


                case "Validacion": 
                    this.JugandoPartida = false;
                    this.Estadisticas = false;
                    this.Jugar = false;
                    this.Jugadores = false;
                    this.Validacion = true;
                    this.loader = false;
                break;
                

                case "Loader": 
                    this.JugandoPartida = false;
                    this.Estadisticas = false;
                    this.Jugar = false;
                    this.Jugadores = false;
                    this.Validacion = false;
                    this.loader = true;
                break;

                case "LoaderInfoBalota": 
                    this.LoaderInfoBalota = true;
                break;


                
            }
        },


        MostrarAlertas: function(tipo, estado){
            if(tipo == "registrarJugador"){
                if(estado == "camposVacios"){
                    if(this.nombre == ""){this.ALert_nombre = "border-danger";}
                    if(this.num_cedula == ""){this.Alert_num_cedula = "border-danger";}
                    if(this.direccion == ""){this.Alert_direccion = "border-danger";}
                    if(this.telefono == ""){this.Alert_telefono = "border-danger";}
                    if(this.numero_tabla == ""){this.Alert_numero_tabla = "border-danger";}
                }else if(estado == "ok"){
                    this.AlertJugadorRegistrado = true;
                }else if(estado == "NumTablaNoEncontrado"){
                    this.AlertNumTablaNoEncontrado = true;
                    this.Alert_numero_tabla = "border-danger";
                }else if(estado == "NumTablaTomado"){
                    this.Alert_numero_tabla = "border-danger";
                    this.AlertTablaTomada = true;
                }else if(estado == "JugadorExistente"){
                    this.Alert_num_cedula = "border-danger";
                    this.AlertJugadorExistente = true;
                }
            }else if(tipo == "CrearPartida"){
                if(estado == "ok"){
                    this.HabilitarComponente("Jugar");
                }
            }else if(tipo == "IngresarBalota"){
                if(estado == "CamposVacios"){
                    if(this.LetraBalota ==""){this.Alert_LetraBalota = "border-danger";}
                    if(this.NumeroBalota == ""){this.Alert_NumeroBalota = "border-danger";}
                }
            }
        },


        habilitarSelectBuscadorTablas : function() {
            $('.buscador_tablas').select2();
        }


    },
    
    mounted() {
        //this.habilitarSelectBuscadorTablas();
        this.cargarTablas();
        this.CargarPartidas();
    },
})


