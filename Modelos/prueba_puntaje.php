<?php
require_once "conexion.php";

$stmt = Conection::conectar()->query("SELECT j.num_identificacion, j.nombre, j.direccion, j.numero_tabla, (
    SELECT Count(*)
    FROM balotas_partidas, tablas 
    WHERE 
        tablas.codigo_tabla = j.numero_tabla
    AND
        (
            balotas_partidas.balota_letra = 'B' 
            AND 
            (
                balotas_partidas.balota_numero = tablas.b
                OR
                balotas_partidas.balota_numero = tablas.bb
                OR
                balotas_partidas.balota_numero = tablas.bbb
                OR
                balotas_partidas.balota_numero = tablas.bbbb
                OR
                balotas_partidas.balota_numero = tablas.bbbbb
            )
        ) OR
        (
            balotas_partidas.balota_letra = 'I' 
            AND 
            (
                balotas_partidas.balota_numero = tablas.i
                OR
                balotas_partidas.balota_numero = tablas.ii
                OR
                balotas_partidas.balota_numero = tablas.iii
                OR
                balotas_partidas.balota_numero = tablas.iiii
                OR
                balotas_partidas.balota_numero = tablas.iiiii
            )
        ) OR
        (
            balotas_partidas.balota_letra = 'N' 
            AND 
            (
                balotas_partidas.balota_numero = tablas.n
                OR
                balotas_partidas.balota_numero = tablas.nn
                OR
                balotas_partidas.balota_numero = tablas.nnnn
                OR
                balotas_partidas.balota_numero = tablas.nnnnn
            )
        ) OR
        (
            balotas_partidas.balota_letra = 'G' 
            AND 
            (
                balotas_partidas.balota_numero = tablas.g
                OR
                balotas_partidas.balota_numero = tablas.gg
                OR
                balotas_partidas.balota_numero = tablas.ggg
                OR
                balotas_partidas.balota_numero = tablas.gggg
                OR
                balotas_partidas.balota_numero = tablas.ggggg
            )
        ) OR
        (
            balotas_partidas.balota_letra = 'O' 
            AND 
            (
                balotas_partidas.balota_numero = tablas.o
                OR
                balotas_partidas.balota_numero = tablas.oo
                OR
                balotas_partidas.balota_numero = tablas.ooo
                OR
                balotas_partidas.balota_numero = tablas.oooo
                OR
                balotas_partidas.balota_numero = tablas.ooooo
            )
        )
) as puntaje
FROM jugadores as j, partidas
WHERE partidas.id = " . $_GET["id_partida"] . " ORDER BY puntaje DESC LIMIT 10" );
//$stmt->bindParam(":id_partida",$_GET["id_partida"], PDO::PARAM_INT);

//$stmt->execute([$_GET["id_partida"]]);

//$results = $stmt->fetch();
while ($row = $stmt->fetch()) {
    echo $row['nombre'] . " - " . $row['puntaje'] . "<br />\n";
}

//var_dump($results);

