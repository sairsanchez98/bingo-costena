<?php
require_once "../../Modelos/mdlPartidas.php";
require_once "../../Modelos/mdlJugadores.php";
require_once "../../Modelos/tablas.php";
require_once "../../ext/carbon/vendor/autoload.php";
use Carbon\Carbon;
date_default_timezone_set('America/Bogota');
Carbon::setLocale('es');
$fechaActual = Carbon::now()->toDateTimeString();




if (isset($_GET["CargarPartidas"])) {
    if($_GET["CargarPartidas"] == "CargarTodo"){
         //$partidas = mdlPartidas::CargarPartidas(null, null, "DESC", "id");
        $data = file_get_contents("../../Modelos/DB/DB_partidas.json");
        $partidas = json_decode($data, true);
    }else if ($_GET["CargarPartidas"] == "CargarPartida") {
        $data = file_get_contents("../../Modelos/DB/DB_partidas.json");
        $partidas_ = json_decode($data, true);
        //$partidas = mdlPartidas::CargarPartidas("id", $_GET["id_partida"], "DESC", "id");
        $partidas = array();
        foreach($partidas_ as $partida){
            if ($partida["id"] == $_GET["id_partida"]) {
                array_push($partidas , $partida);
                break;
            }
        }
    }

    $estilosBadge = array(
        "creada" => "btn btn-success",
        "jugando" => "btn btn-primary",
        "finalizada" => "btn btn-warning"
    );

    $partidasDB = array();

    foreach($partidas as $partida){
        $p = array(
        "estado" => $partida["estado"],
        "fecha_registro" => $partida["fecha_registro"],
        "id" => $partida["id"],
        "titulo" => $partida["titulo"],
        "class_estilo" => $estilosBadge[$partida["estado"]]
        );

        array_push($partidasDB, $p);
    }

    $rest["respuesta"] = $partidasDB;

    header("Content-Type: application/json");
    echo json_encode($rest);
}




// forma 1 de cargar las jugadas y balotas de una partida
if (isset($_GET["CargarJugadasPartidas"])) {
    $jugadasPartidas = mdlPartidas::CargarJugadasPartidas("id_partida", $_GET["id_partida"],null , null);
    $balotasJugadas = array();
    foreach($jugadasPartidas as $Balotajugada){
        array_push($balotasJugadas , $Balotajugada["balota_letra"].$Balotajugada["balota_numero"] );
    }

    $balotasJugadas = array_unique($balotasJugadas); // hay que unificar para que no se repitan 
    $balotasJugadasFull = array(); // aaquí se almacena la info de todas las balotas 

    // saco toda la información de las balotas
    foreach($balotasJugadas as $balota){
        $balota_letra  = substr($balota, 0, 1);
        $balota_numero = substr($balota, 1);

        //$consultaBalotaPartida = mdlPartidas::InfoBalotas($balota_letra, $balota_numero, $_GET["id_partida"]);
        $consultaBalotaPartida = array();
        ## encapsulamos todas las jugadas que pertenecen a cada balota
        foreach($jugadasPartidas as $Balotajugada){
            if ($Balotajugada["balota_letra"] == $balota_letra 
            && $Balotajugada["balota_numero"] == $balota_numero) {
                array_push($consultaBalotaPartida, $Balotajugada);
            }
        }
        
        // cargar jugadores  que pertenecen a esas balotas : :: :
        $jugadores_ = array();
        foreach($consultaBalotaPartida as $cbp){ 
            $consultaJugadores = mdlJugadores::CargarJugadores("numero_tabla", $cbp["numero_tabla"], "DESC", "id");
            foreach($consultaJugadores as $jugador){
                array_push($jugadores_, $jugador);
            }
        }


        
        $balotaFull = array(
            "letra" => $balota_letra,
            "numero" => $balota_numero,
            "num_tablas" => count($consultaBalotaPartida),
            "num_jugadores" => count($jugadores_)
        );

        array_push($balotasJugadasFull,  $balotaFull);
    }

    //$rest["jugadas"] = $jugadasPartidas; ## JUGADAS COMPLETAS
 
    //$rest["BalotasJugadas"] = $balotasJugadas; ## SOLO LAS BALOTAS

    $rest["BalotasFull"] = $balotasJugadasFull;

    $rest["jugadores"] = $jugadores_; 
    
    $rest["ok"] = "ok";
    


    header("Content-Type: application/json");
    echo json_encode($rest);
}


// forma 2 de cargar las jugadas y balotas de una partida

if (isset($_GET["CargarJugadasPartidas_"])) {
    $jugadasPartidas = mdlPartidas::CargarJugadasPartidas_("id_partida", $_GET["id_partida"],null , null);

    $rest["balotas"] = $jugadasPartidas;
    header("Content-Type: application/json");
    echo json_encode($rest);
}




if (isset($_GET["cargar_jugadores_de_una_balota"])) {
    $consultaBalotaPartida = mdlPartidas::InfoBalotas($_GET["letra"], $_GET["numero"], $_GET["id_partida"]);
        
    // cargar jugadores  que pertenecen a esas balotas : :: :
    $jugadores_ = array();
    foreach($consultaBalotaPartida as $cbp){ 
        $consultaJugadores = mdlJugadores::CargarJugadores("numero_tabla", $cbp["numero_tabla"], "DESC", "id");
        foreach($consultaJugadores as $jugador){
            array_push($jugadores_, $jugador);
        }
    }

    $rest["jugadores"] = $jugadores_; 

    header("Content-Type: application/json");
    echo json_encode($rest);
}




if (isset($_POST["crearPartida"])) {

    ## CUANDO SE CREA UNA NUEVA PARTIDA DEBEMOS FORMATEAR TODOS LOS ARCHIVOS JSON
    ## ARCHIVOS JSON: 
    $DB_JSON =  array(
        "DB_balotas_analizadas_lote1.json","DB_balotas_analizadas_lote2.json",
        "DB_balotas_analizadas_lote3.json","DB_balotas_analizadas_lote4.json",
        "DB_balotas_analizadas_lote5.json","DB_balotas_analizadas_lote6.json",
        "DB_balotas_analizadas_lote7.json","DB_balotas_analizadas_lote8.json",
        "DB_balotas_analizadas_lote9.json","DB_balotas_analizadas_lote10.json",

        "DB_tablas_cantadas_lote1.json", "DB_tablas_cantadas_lote2.json",
        "DB_tablas_cantadas_lote3.json", "DB_tablas_cantadas_lote4.json",
        "DB_tablas_cantadas_lote5.json", "DB_tablas_cantadas_lote6.json",
        "DB_tablas_cantadas_lote7.json", "DB_tablas_cantadas_lote8.json",
        "DB_tablas_cantadas_lote9.json", "DB_tablas_cantadas_lote10.json",

        "DB_balotas_partidas.json", "DB_partidas.json"
    );
    foreach ($DB_JSON as $key => $dbjson) {
        $FORMATO = Array(); // array en cero 
        $jsonencoded = json_encode($FORMATO,JSON_UNESCAPED_UNICODE);
        $registrar = fopen("../../Modelos/DB/".$dbjson, 'w');
        fwrite($registrar, $jsonencoded);
        fclose($registrar);
    }

    


    ### traigo a las partidas que están actualmente registradas
    $data = file_get_contents("../../Modelos/DB/DB_partidas.json");
    $partidas = json_decode($data, true);
    if (count($partidas) > 0) {
        foreach($partidas as $pDB){}
        $id_nueva_partida = $pDB["id"] + 1; ## id de la nueva partida;
        $datos = array(
        
            "id" =>  $id_nueva_partida,
            "titulo" => $_POST["titulo"],
            "estado" => "creada",
            "fecha_registro" => $fechaActual
    
        );
    }else{
        $datos = array(
        
            "id" => 1,
            "titulo" => $_POST["titulo"],
            "estado" => "creada",
            "fecha_registro" => $fechaActual
    
        );
    }
    // creo el nuevo obj partida: 
   


    //$registrar = mdlPartidas::crearPartida($datos);
    
    
    

    ## ahora ingreso la nueva partida: 
    array_push($partidas , $datos);


    ## guardo el archivo json:
    $jsonencoded = json_encode($partidas,JSON_UNESCAPED_UNICODE);
    $registrar = fopen("../../Modelos/DB/DB_partidas.json", 'w');
    fwrite($registrar, $jsonencoded);
    fclose($registrar);

    if ($registrar) {
        // activo el cron
        
        //header('https://www.xn--tucostea-j3a.com/soft981129-bingo/Modelos/Cron.php');

        $rest["respuesta"] = "ok";
    }


    header("Content-Type: application/json");
    echo json_encode($rest);
}



















if (isset($_POST["ingresar_balota"])) {
    ## vcargo las balotas 
    $data = file_get_contents("../../Modelos/DB/DB_balotas_partidas.json");
    $balotas_partidas = json_decode($data, true);
    foreach($balotas_partidas as $DBBP){}
    $id_nueva_balota   =  $DBBP["id"] + 1;


    $datos = array(
        "id" => $id_nueva_balota,
        "numero" => $_POST["numero"],
        "letra" => $_POST["letra"],
        "id_partida" => $_POST["id_partida"],
        "fecha_juego" => $fechaActual
    );

    ## si es una partida con estado de "creada" quiere decir que esta es su primer balota y por lo tanto 
    ## debemos cambiar el estado a "jugando"

    

### si la partida es pirmera vez que recibe una balota:
        if ($_POST["estado_partida"] == "creada") {
            $data = file_get_contents("../../Modelos/DB/DB_partidas.json");
            $partidas = json_decode($data, true);
            foreach($partidas as  $partida){
                if ($partida["id"] == $datos["id_partida"]) {
                    ## le cambiamos el estado a dicha partida:
                    $partida["estado"] = "jugando";
                    ## guardo el archivo json:
                
                    // $newJsonString = json_encode($partidas);
                    // file_put_contents('../../Modelos/DB/DB_partidas.json', $newJsonString);
                }
            }
            //$actualizar_estado = mdlPartidas::CambiarEstadoPartida($datos["id_partida"], "jugando");
        }


    ## ingresamos la nueva balota al json: 
    array_push($balotas_partidas , $datos);   
     ## guardo el archivo json:
     $jsonencoded = json_encode($balotas_partidas,JSON_UNESCAPED_UNICODE);
     $registrar = fopen("../../Modelos/DB/DB_balotas_partidas.json", 'w');
     fwrite($registrar, $jsonencoded);
     fclose($registrar); 


    //$IngresarBalota = mdlPartidas::IngresarBalota($datos);




  


    //$rest["balota"] = $datos;
    //$rest["tablas"] = $tablas_que_tienen_la_balota;
    $rest["id_partida"] = $datos["id_partida"];

    header("Content-Type: application/json");
    echo json_encode($rest);
    
}