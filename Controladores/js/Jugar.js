var  Jugar = new Vue({
    el: "#jugar",
    data: {
        partidasDB : [],
        NUmpartidasExistentes: "",
        NombreNuevaPartida: "",
        Alert_Nombre_Partida  : "border-default",
        PartidaSeleccionada :[],
        BalotasJugadasPartida: [],
        BalotaSeleccionada : [],

        // ingresar balota: : 
        existeBalota: false,
        NumeroBalota: "",
        LetraBalota: "",
        Alert_NumeroBalota: "border-default",
        Alert_LetraBalota: "border-default",
        DatosBalota: [], // aquí se almacenará la información de una balota cuando sea requrido

        loader: false
    },
    methods: {
        CargarPartidas: function(){
            axios.get("Controladores/php/ctrlPartidas.php?CargarPartidas=CargarTodo")
            .then(function(response){
                let datos = response.data;
                Jugar.partidasDB = datos.respuesta;
                Jugar.NUmpartidasExistentes = datos.respuesta.length;
                console.log(response);
            })
        },

        RegistrarNuevaPartida : function (){
            
            formdata = new FormData();
            formdata.append("crearPartida", "ok");
            formdata.append("titulo", this.NombreNuevaPartida);

            axios.post("Controladores/php/ctrlPartidas.php", formdata).then(function(response){
                let datos =  response.data;
                if(datos.respuesta == "ok"){
                    Jugar.CargarPartidas();  
                    Jugar.NombreNuevaPartida = "";
                }
                console.log(response);
            })
        },

        BalotaInfo: function(letra, numero, id_partida){
            
            window.location="index.php?rute=jugar&id_partida="+id_partida+
            "&balota-seleccionada=ok&letra="+letra+"&numero="+numero
        },

        JugarPartida: function(id){
            window.location="index.php?rute=jugar&id_partida="+id;
        },

        IngresarBalota: function(){
            this.Loader = true;
            //this.ValidarExistenciaDeBalota(this.LetraBalota , this.NumeroBalota);
            let estado_partida = document.getElementById("estado_partida_").value;
            let id_partida = document.getElementById("id_partida_").value;
           if(this.LetraBalota !== "" && this.NumeroBalota !== "" ){
            formdata = new FormData();
            formdata.append("ingresar_balota", "ok");
            formdata.append("letra", this.LetraBalota);
            formdata.append("numero", this.NumeroBalota);
            formdata.append("id_partida", id_partida);
            // envio el estado para que php lo verifique si el estado es "creada" entonces deberá pasar
            // a "jugando"
            formdata.append("estado_partida", estado_partida);

            axios.post("Controladores/php/ctrlPartidas.php",formdata)
            .then(function(response){
                let datos  = response.data;
                console.log(response);
                window.location="index.php?rute=jugar&id_partida="+id_partida;
                Jugar.LetraBalota = "";
                Jugar.NumeroBalota = "";
                
                
                
            })
           }else{
            
           }
        },
    },

    mounted(){
        this.CargarPartidas();
    }

})