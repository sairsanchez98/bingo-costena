<?php
require_once "conexion.php";


class mdlPartidas{

    static public function CargarPartidas($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  partidas WHERE $item = $value ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM partidas ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM partidas ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }


    static public function crearPartida($datos){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        partidas(fecha_registro, titulo, estado) 
        VALUES (:fecha_registro, :titulo, :estado)");
        $stmt->bindParam(":fecha_registro", $datos["fecha_registro"], PDO::PARAM_STR);
        $stmt->bindParam(":titulo", $datos["titulo"], PDO::PARAM_STR);
        $stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);
        
        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
        $stmt = null;    
    }



    static public function CargarJugadasPartidas($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  jugadas_partidas WHERE $item = $value ORDER BY id DESC ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadas_partidas ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadas_partidas ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }


    static public function CargarTablasCantadas($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  tablas_cantadas WHERE $item = $value ORDER BY $itemOrden DESC  LIMIT 10");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM tablas_cantadas ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM tablas_cantadas ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }



    static public function CargarJugadasPartidas_($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  balotas_partidas WHERE $item = $value ORDER BY id DESC ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM balotas_partidas ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM balotas_partidas ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }



    static public function CambiarEstadoPartida($id_partida, $estado){
        $stmt = Conection::conectar()->prepare("UPDATE partidas 
        SET estado = :estado WHERE id = :id_partida ");

        $stmt->bindParam(":id_partida", $id_partida, PDO::PARAM_INT);
        $stmt->bindParam(":estado", $estado, PDO::PARAM_STR);
        
        
        
        if($stmt->execute()){

			return true;

		}else{

			return false;
		
		}

    }



    static public function IngresarBalotaJugadaEnPartida($datos, $tabla){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        jugadas_partidas(numero_tabla, balota_numero, balota_letra, id_partida) 
        VALUES ( :numero_tabla, :balota_numero, :balota_letra, :id_partida)");
        $stmt->bindParam(":numero_tabla", $tabla, PDO::PARAM_STR);
        $stmt->bindParam(":balota_numero", $datos["numero"], PDO::PARAM_STR);
        $stmt->bindParam(":balota_letra", $datos["letra"], PDO::PARAM_STR);
        $stmt->bindParam(":id_partida", $datos["id_partida"], PDO::PARAM_INT);

        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
        $stmt = null;    
    }





    static public function IngresarBalota($datos){
        $stmt = Conection::conectar()->prepare("INSERT INTO 
        balotas_partidas(balota_letra, balota_numero, id_partida, fecha_juego) 
        VALUES (:balota_letra, :balota_numero, :id_partida, :fecha_juego)");
        
        
        $stmt->bindParam(":balota_letra", $datos["letra"], PDO::PARAM_STR);
        $stmt->bindParam(":balota_numero", $datos["numero"], PDO::PARAM_STR);
        $stmt->bindParam(":id_partida", $datos["id_partida"], PDO::PARAM_INT);
        $stmt->bindParam(":fecha_juego", $datos["fecha_juego"], PDO::PARAM_STR);

        if($stmt->execute()){
		  	return true;
        }else{
          return false;
        }
        $stmt->close();
        $stmt = null;    
    }


    static public function InfoBalotas($letra, $numero, $id_partida){
        
        $conn = Conection::conectar()->prepare("SELECT * FROM jugadas_partidas 
        WHERE balota_letra = '$letra' AND balota_numero = '$numero' AND id_partida = '$id_partida' ");
        $conn -> execute();
        return $conn->fetchAll();
        
    }

    static public function CargarJugadas($item, $value, $orden, $itemOrden){
        if ($item !== null) {
            $conn = Conection::conectar()->prepare("SELECT * FROM  jugadas_partidas WHERE $item = $value ORDER BY id DESC ");
            $conn -> execute();
            return $conn->fetchAll();
        }else{
            if ($orden == "ASC") {
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadas_partidas ORDER BY $itemOrden ASC ");
                $conn -> execute();
                return $conn->fetchAll();
            }else{
                $conn = Conection::conectar()->prepare("SELECT * FROM jugadas_partidas ORDER BY $itemOrden DESC ");
                $conn -> execute();
                return $conn->fetchAll();
            }
        }
    }

}