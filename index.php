<?php ?>

<!doctype html>
<html lang="es">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="Assets/jquery.min.js"></script>


    <script src="Assets/Vue.js"></script>
    <script src="Assets/Axios.js"></script>
    
  </head>


  <body>
    
    <div class="container-fluid" id="Bingo">

      <!-- IMAGEN CABECERA -->
        <div class="d-flex justify-content-center">
            <img src="dist/img/header.gif" class="img-fluid" alt="Responsive image">
        </div>


      <!-- MENÚ -->
        <div class="d-flex  justify-content-center">
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" >
               <a href="estadisticas" class="nav-link 
               <?php if(@$_GET["rute"] == 'estadisticas' || @$_GET["rute"] == ''){echo "active";} ?>" 
                aria-selected="true">Estadísticas</a>
            </li>
            <li class="nav-item" >
               <a href="jugar" class="nav-link <?php if(@$_GET["rute"] == 'jugar'){echo "active";} ?>"    aria-selected="true">Jugar</a>
            </li>
            <li class="nav-item" >
               <a href="jugadores" class="nav-link <?php if(@$_GET["rute"] == 'jugadores'){echo "active";} ?>"    aria-selected="true">Jugadores</a>
            </li>
        
          </ul>
        </div>


        <!-- LOADER -->
        <!-- <div class="container-fluid" v-if="loader">
            <div class="d-flex justify-content-center">
                <img src="dist/img/preloader.gif" class="img-fluid" alt="Responsive image">
            </div>
        </div> -->

        <?php 
            require_once"Controladores/php/ctrl_vistas.php";
        ?>
        
        
    </div>


    


    
    <!-- <script src="Controladores/js/Main.js"></script> -->

    

     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> 
  
    
    
    
  </body>
</html>



